-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3308
-- Generation Time: Oct 15, 2016 at 10:07 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atmajaya_hme`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `idBerita` int(4) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `divisi` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `dibaca` int(6) NOT NULL DEFAULT '0',
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`idBerita`, `judul`, `isi`, `divisi`, `gambar`, `dibaca`, `tanggal`) VALUES
(1, 'Pengenalan Jurusan Teknik Elektro 2016', 'Kampus UNIKA Atma Jaya, Jakarta - Himpunan Mahasiswa Elektro Unika Atma Jaya mengadakan acara "Pengenalan Jurusan Teknik Elektro 2016". Acara ini bertujuan untuk mengenalkan serba-serbi kehidupan di dalam Fakultas Teknik Elektro Unika Atma Jaya', '', '1472034123116.jpg', 11, '2016-09-14'),
(2, 'Studi Banding Internal HME FT-UAJ dengan HIMAPSI FP-UAJ', 'Kampus UNIKA Atma Jaya, Jakarta -  Himpunan Mahasiswa Elektro UNIKA Atma Jaya (HME FT-UAJ) menjalankan salah satu program kerjanya pada tanggal 24 Agustus 2016. Program kerja tersebut merupakan Studi Banding Internal dengan Himpunan Mahasiswa Psikologi (HIMAPSI FP-UAJ) UNIKA Atma Jaya. Studi banding dilaksanakan di Kampus Semanggi UNIKA Atma Jaya, tepatnya di gedung Yustinus kelas Yustinus Basement 208. Studi Banding dilakukan untuk memperkenalkan masing-masing himpunan dan untuk saling bertukar pikiran dalam struktur organisasi dan program kerja.\r\n\r\nSebelum memulai rapat, dilakukan sesi pengenalan satu dengan lainnya. Pengenalan dilakukan melalui bermain games "Do you love me?". Suasana sangat menyenangkan!!!\r\n\r\nSetelah sesi pengenalan, barulah masuk ke sesi utama. Kedua himpunan saling memperkenalkan program kerja dan kinerjanya masing-masing dengan cara presentasi. Presentasi pertama dilakukan oleh HIMAPSI yang kemudian dilanjutkan oleh HME. \r\n\r\nSetelah kedua himpunan selesai melakukan presentasi, dimulailah sesi tanya jawab perihal program kerja maupun administrasi masing-masing himpunan.\r\n\r\nAkhir acara ditutup dengan doa yang kemudian diikuti oleh foto-foto bersama. Semoga Studi Banding Internal ini berguna untuk membangun hubungan baik antara kedua himpunan untuk memajukan kualitas UNIKA Atma Jaya. Terima kasih HIMAPSI FP-UAJ! Ditunggu kerja sama selanjutnya! -JJ', '', '14102328_200844350330558_4200301391306533634_n.jpg', 23, '2016-09-15'),
(3, 'Meet n'' Greet', 'Ruang Multimedia Teknik, Atma Jaya - Acara internal HME "Meet n'' Greet" pada hari Rabu, 14 September 2016 dilaksanakan. Acara kali ini akan memperkenalkan mahasiswa Teknik Elektro 2016 dengan organisasi mahasiswa (ormawa) yang ada di Teknik Elektro Atma Jaya, yang tak lain adalah Himpunan Mahasiswa Elektro (HME) dan Workshop Elektro (WSE). Acara diikuti oleh seluruh mahasiswa teknik elektro 2016 dan seluruh anggota HME dan WSE. Acara dimulai dengan doa yang dipimpin oleh Mahasiswa Elektro 2016. Kemudian, dimulai dengan Ice Breaking dengan bermain "do you love me?". Setelah suasana mencair, barulah HME dan WSE mulai memperkenalkan organisasinya kepada para peserta. Dimulai dengan HME kemudian disusul dengan WSE. Pada akhir acara, tidak lupa para panitia HME Nite 2016 mempromosikan acaranya kepada para peserta acara khususnya kepada mahasiswa elektro 2016. -JJ', '', '1473954013222 (Medium).jpg', 11, '2016-09-15'),
(4, 'Konsolidasi HME WSE 2016-2017 ', 'Villa Sahabat, Abu Bakar - Himpunan Mahasiswa Elektro (HME) mengadakan acara konsolidasi pada tanggal 5-7 Agustus 2016 sebagai titik awal sebelum serangkaian acara HME dilaksanakan pada periode 2016-2017. Bentuk acaranya diisi dengan berbagai games dan makan bersama, acara ini bertujuan untuk saling mengenal dan mengakrabkan antar pengurus baru sehingga dalam pelaksanaan setiap acaranya dapat terjalin kerja sama yang baik.  ', 'Infrastruktur', '1473952699005.jpg', 5, '2016-09-19'),
(5, 'Unika Atma Jaya mencari mahasiswa unggul di 20 Program Studi S1', 'Unika Atma Jaya mencari mahasiswa unggul di 20 Program Studi S1.\r\n\r\nBerani mencoba?\r\nTanpa tes, cukup dengan menunjukkan nilai rapormu.\r\n\r\nCaranya gampang:\r\n1. Daftar online di pmb.atmajaya.ac.id\r\n2. Sedang duduk di kelas XII\r\n3. Nilai rapor kelas X dan XI sesuai dengan persyaratan.\r\n\r\nCatat tanggalnya ya:\r\nDaftar online: 15 Agustus-12 Oktober\r\nDaftar offline: 10-15 Oktober\r\nPengumuman: 31 Oktober \r\nKonfirmasi: 31 Oktober-12 November\r\n\r\nInfo: \r\npr@atmajaya.ac.id\r\n081517094964 (Nina)\r\n08118951987 (Zeindy)\r\n\r\nFB: Unika Atma Jaya\r\nIG: unikaatmajaya\r\nT: @UnikaAtmaJaya\r\nLine: unikaatmajaya\r\n\r\n#terpercaya kualitas lulusannya\r\n\r\nSalam,\r\nMarketing Public Relations dan Admisi Unika Atma Jaya', '', 'Atma Jaya.jpg', 5, '2016-09-20'),
(6, 'Fun Day Elektro 2016', 'Grand Futsal, Kuningan - Himpunan Mahasiswa Elektro (HME) mengadakan acara fun day pada tanggal 7 Oktober 2016, acara ini bertujuan untuk mendekatkan angkatan aktif mahasiswa elektro melalui olahraga bersama yaitu futsal dan basket, kegiatan ini juga sebagai refreshing kami setelah menjalani UTS. So, we can enjoy and happy together :)', 'Infrastruktur', 'Funday 2016-10-07_4594.jpg', 5, '2016-10-09');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `idEvent` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `divisi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`idEvent`, `judul`, `tanggal`, `tempat`, `gambar`, `keterangan`, `divisi`) VALUES
(2, 'Konsolidasi Pengurus HME dan WSE 2016', '2016-08-05', 'Villa Abu Bakar', 'windows-black-wallpapers.jpg', 'Acara untuk mempererat HME dengan WSE', ''),
(3, 'Pengenalan Kampus Mahasiswa Baru Unika Atma Jaya Utamakan Implementasi Nilai', '2016-08-01', 'Sport Hall Unika Atmajaya', 'Pengenalan-Kampus-2016-2.jpg', '', ''),
(4, 'Kuliah Umum Computational Intelligence & Robotic oleh Dosen Tamu Prof. Pitoyo Hartono, Chukyo Univer', '2016-03-28', 'Ruang Seminar Gedung K2 Lantai 2', 'teknik-EL-flyerkulum12016.jpg', 'Prodi Teknik Elektro bekerja sama dengan Chukyo University Jepang mengadakan Kuliah Umum Computation', ''),
(5, 'STUDI KOMPARATIF INTERNAL 1', '2016-08-23', 'Atma Jaya', '1471950642533.jpg', 'HME Atma Jaya akan melakukan studi banding ke Himpunan Mahasiswa Psikologi pada hari Rabu, 26/8/16', 'Eksternal'),
(6, 'MEET n GREET', '2016-09-13', 'Ruang Multimedia Gedung K1, UNIKA Atma Jaya Jakarta', 'warmwelcome.jpg', 'Come join us in this first road on Invernal Division''s Program Event!!', 'Internal'),
(7, 'Fun Day ', '2016-10-07', 'Grand Futsal ', 'Capture.JPG', 'Yuk ikutan refreshing bareng sama keluarga elektro :)', 'Infrastruktur');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `idGaleri` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `divisi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`idGaleri`, `gambar`, `caption`, `tanggal`, `divisi`) VALUES
(13, 'up1.jpg', 'Pengenalan Jurusan Elektro 2016', '2016-09-13', ''),
(14, 'up2.jpg', 'Pengenalan Jurusan Elektro 2016', '2016-09-13', ''),
(15, 'up3.jpg', 'Pengenalan Jurusan Elektro 2016', '2016-09-13', ''),
(16, 'up4.jpg', 'Pengenalan Jurusan Elektro 2016', '2016-09-13', ''),
(17, 'up5.jpg', 'Pengenalan Jurusan Elektro 2016', '2016-09-13', ''),
(18, 'up6.jpg', 'Pengenalan Jurusan Elektro 2016', '2016-09-13', ''),
(21, 'Konsolidasi HME WSE _2052.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(26, 'Konsolidasi HME WSE _8657.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(27, 'Konsolidasi HME WSE _9955.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(28, 'Konsolidasi HME WSE _4343.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(29, 'Konsolidasi HME WSE _3387.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(32, 'Konsolidasi HME WSE _9932.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(33, 'Konsolidasi HME WSE _9722.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(34, 'Konsolidasi HME WSE _9218.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(35, 'Konsolidasi HME WSE _814.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(36, 'Konsolidasi HME WSE _7019.jpg', 'Konsolidasi HME WSE 2016-2017 ', '2016-09-15', 'Infrastruktur'),
(37, '_DSC1006.JPG', 'Meet and Greet', '2016-09-15', ''),
(38, '_DSC1010.JPG', 'Meet and Greet', '2016-09-15', ''),
(39, '_DSC1012.JPG', 'Meet and Greet', '2016-09-15', ''),
(40, '_DSC1019.JPG', 'Meet and Greet', '2016-09-15', ''),
(41, '_DSC1026.JPG', 'Meet and Greet', '2016-09-15', ''),
(43, '_DSC1058.JPG', 'Meet and Greet', '2016-09-15', ''),
(44, '_DSC1070.JPG', 'Meet and Greet', '2016-09-15', ''),
(45, '_DSC1062.JPG', 'Meet and Greet', '2016-09-15', ''),
(46, '_DSC1083.JPG', 'Meet and Greet', '2016-09-15', ''),
(47, 'Funday 2016-10-07_8088.jpg', 'Fun Day Elektro 2016', '2016-10-09', 'Infrastruktur'),
(48, 'Funday 2016-10-07_5143.jpg', 'Fun Day Elektro 2016 ', '2016-10-09', 'Infrastruktur'),
(49, 'Funday 2016-10-07_2353.jpg', 'Fun Day Elektro 2016 ', '2016-10-09', 'Infrastruktur'),
(50, 'Funday 2016-10-07_2030.jpg', 'Fun Day Elektro 2016 ', '2016-10-09', 'Infrastruktur'),
(51, 'Funday 2016-10-07_4060.jpg', 'Fun Day Elektro 2016 ', '2016-10-09', 'Infrastruktur'),
(52, 'Funday 2016-10-07_4594.jpg', 'Fun Day Elektro 2016 ', '2016-10-09', 'Infrastruktur');

-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

CREATE TABLE `kelompok` (
  `kd_kelompok` int(11) NOT NULL,
  `nama_kelompok` varchar(100) NOT NULL,
  `fk_sekolah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelompok`
--

INSERT INTO `kelompok` (`kd_kelompok`, `nama_kelompok`, `fk_sekolah`) VALUES
(1, 'Atma 1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `kd_peserta` int(11) NOT NULL,
  `nama_peserta` varchar(100) NOT NULL,
  `telp_peserta` varchar(40) NOT NULL,
  `foto_peserta` varchar(40) NOT NULL,
  `fk_kelompok` int(11) DEFAULT NULL,
  `fk_sekolah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`kd_peserta`, `nama_peserta`, `telp_peserta`, `foto_peserta`, `fk_kelompok`, `fk_sekolah`) VALUES
(1, 'Juminten', '0896551265', '250px-349Feebas.png', 1, 1),
(2, 'Temmie', '0814517512', 'tem.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE `sekolah` (
  `kd_sekolah` int(11) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `user_pass` varchar(100) DEFAULT NULL,
  `nama_sekolah` varchar(100) NOT NULL,
  `telp_sekolah` varchar(30) DEFAULT NULL,
  `alamat_sekolah` varchar(100) DEFAULT NULL,
  `nama_guru` varchar(100) DEFAULT NULL,
  `telp_guru` varchar(30) DEFAULT NULL,
  `bukti_bayar` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sekolah`
--

INSERT INTO `sekolah` (`kd_sekolah`, `user_id`, `user_pass`, `nama_sekolah`, `telp_sekolah`, `alamat_sekolah`, `nama_guru`, `telp_guru`, `bukti_bayar`) VALUES
(1, 'atma', 'cd30f109bf09fe16b7e58d1316d7f919', 'Sekolah Atmajaya', '65176129', 'Semanggi', 'Temmie', '0897154154', '20150303_141059.jpg'),
(2, 'UjungKulon', 'aa72c6f7745fa8b7d1da1c34a9b0c1fe', 'UjungKulon', '612547165', 'Di Ujung', 'Si Kulon', '62516745', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `noid` int(4) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `divisi` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`noid`, `username`, `password`, `nama`, `email`, `divisi`, `gambar`) VALUES
(2, 'admin', '0192023a7bbd73250516f069df18b500', 'Administrator', 'hme.ftuaj@gmail.com', '', 'HME.jpg'),
(3, 'eksternal', '07354c81e2c0d8e28267c4dbcc4fc47b', 'Admin Eksternal', 'eksternal@gmail.com', 'Eksternal', 'Scholar.jpg'),
(4, 'internal', '91ba1d487f877ce42093e551c5f2c82e', 'Admin Internal', 'internal@gmail.com', 'Internal', 'Facebook Profile Image 180 x 180 px Ravenclaw.png'),
(5, 'infrastruktur', '3fc5ba23b0e7c1c4b8dbf310608d3d26', 'Admin Infrastruktur', 'infrastruktur@gmail.com', 'Infrastruktur', 'Thief.jpg'),
(6, 'e-society', 'da8aff7396ab1099fbcbd451aafff857', 'Admin E-Society', 'e-society@gmail.com', 'E-Society', 'White Mage.jpg'),
(8, 'Machalite', '4349292792ba1efd4762f8296ab60b00', 'Jonathan', 'jonathanhuang7070@gmail.com', 'E-Society', 'chocoboface.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`idBerita`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`idEvent`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`idGaleri`);

--
-- Indexes for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`kd_kelompok`),
  ADD KEY `fk_sekolah` (`fk_sekolah`);

--
-- Indexes for table `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`kd_peserta`),
  ADD KEY `fk_sekolah` (`fk_sekolah`),
  ADD KEY `fk_kelompok` (`fk_kelompok`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`kd_sekolah`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`noid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `idBerita` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `idGaleri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `kd_kelompok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `peserta`
--
ALTER TABLE `peserta`
  MODIFY `kd_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `kd_sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `noid` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD CONSTRAINT `kelompok_ibfk_1` FOREIGN KEY (`fk_sekolah`) REFERENCES `sekolah` (`kd_sekolah`);

--
-- Constraints for table `peserta`
--
ALTER TABLE `peserta`
  ADD CONSTRAINT `peserta_ibfk_1` FOREIGN KEY (`fk_sekolah`) REFERENCES `sekolah` (`kd_sekolah`),
  ADD CONSTRAINT `peserta_ibfk_2` FOREIGN KEY (`fk_kelompok`) REFERENCES `kelompok` (`kd_kelompok`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

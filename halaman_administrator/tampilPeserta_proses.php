<?php
	include("../koneksi.php");
	$no=1;
	$strqry="select kd_peserta,nama_peserta, foto_peserta, telp_peserta, nama_kelompok, nama_sekolah
			from peserta p, kelompok k, sekolah s
			WHERE p.fk_sekolah=s.kd_sekolah
			AND p.fk_kelompok=k.kd_kelompok";
			 
	$sql=mysqli_query($con,$strqry);


	while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
?>

	<tr>
		<td><?php echo $data['nama_peserta'];?></td>
		<td>
		<?php 
		if($data['foto_peserta']=="")
			$foto="user.png";
		else
			$foto=$data['foto_peserta'];
		?>
		<img src="../../apcom.atmajaya.org/img/apcom/foto/<?php echo $foto?>" width="100px">
		</td>
		<td><?php echo $data['telp_peserta'];?></td>
		<td><?php echo $data['nama_kelompok'];?></td>
		<td><?php echo $data['nama_sekolah'];?></td>
		<td width="150">
			<a href="ubahPeserta.php?&id=<?php echo $data['kd_peserta']; ?>">
				<button type="button" class="btn btn-primary">Ubah</button>
			</a>
			<a href="hapusPeserta.php?&id=<?php echo $data['kd_peserta']; ?>" onClick="return confirm('Hapus peserta ini?')">
				<button type="button" class="btn btn-danger">Hapus</button>
			</a>
		</td>
	</tr>
	
<?php
	$no++;
	}
?>
<?php
	include("koneksi.php");
	
	$no=1;
	
	// attempt select query execution
	$sql=mysqli_query($con,"SELECT * from events order by tanggal desc limit 3");

	while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
		$judul=$data['judul'];
		if(strlen($judul)>100){
			//MEMBATASI JUDUL BERITA YANG DITAMPILKAN SAMPAI 50 HURUF PERTAMA SAJA
			$judul=substr($data['judul'],0,50)+"...";
		
			//MENGAMBIL JUDUL BERITA SAMPAI KETEMU SPASI TERAKHIR SEBELUM 50 HURUF PERTAMA
			$judul=substr($data['judul'],0,strrpos($judul," "))+"...";
		}
		
		
		//MENGATUR FORMAT DATA KELUARAN
		$tanggal=new DateTime($data['tanggal']);
		
		if($no==1){
			echo "<div class='item active'>";
		}else{
			echo "<div class='item'>";
		}
?>
		<div class="container center">
			<div class="col-md-6 col-md-offset-3 slide-custom">		
				<h3>
					<i class="fa fa-quote-left"></i> 
						<?php echo $judul;?><br>
						-<i><?php echo $tanggal->format('l, jS F Y');?></i>-
					<i class="fa fa-quote-right"></i>
				</h3>

				<p class="button">
					<a href="halaman_user/tampilEvent.php">See Details</a>
				</p>
			</div>
		</div>
	</div>
	
<?php
	$no++;
	}
?>
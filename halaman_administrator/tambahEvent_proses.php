<?php
	// MENCEGAH USER MASUK MELALUI URL TANPA LOGIN
	session_start();
		
	if(!empty($_SESSION['username']) and !empty($_SESSION['password'])){
		include("../koneksi.php");
		
		// MENDAPATKAN INFORMASI MENGENAI GAMBAR YANG DIUPLOAD (NAMA,LOKASI, DAN TIPE)
		$nama_gambar=$_FILES['gambar']['name'];
		$lokasi_gambar=$_FILES['gambar']['tmp_name'];
		$tipe_gambar=$_FILES['gambar']['type'];
		$divisi=$_SESSION['divisi'];
		$sql;

		//escape user inputs for security
		$judul = mysqli_real_escape_string($con, $_POST['judul']);
		$tanggal=date("Y-m-d", strtotime($_POST['tanggal'])); 
		$tempat = mysqli_real_escape_string($con, $_POST['tempat']);
		$keterangan =mysqli_real_escape_string($con,$_POST['keterangan']);
		
	 
		if($lokasi_gambar==""){
			if($divisi==""){
				//attempt insert query execution
				$sql = "INSERT INTO events (judul,tanggal,tempat,keterangan) VALUES ('$judul','$tanggal','$tempat','$keterangan')";
			}else{
				//attempt insert query execution
				$sql = "INSERT INTO events (judul,tanggal,tempat,keterangan,divisi) VALUES ('$judul','$tanggal','$tempat','$keterangan','$divisi')";
			}
			
			
			if(mysqli_query($con, $sql)){
				echo "<center>Records added successfully.</center>";
			} else{
				echo "<center>ERROR: Could not able to execute $sql.</center>" . mysqli_error($con);
			}
		}else{
			//MEMINDAHKAN GAMBAR YANG DIUPLOAD KE FOLDER IMG
			move_uploaded_file($lokasi_gambar,"../img/uploads/$nama_gambar");
			
			if($divisi==""){
				//attempt insert query execution
				$sql = "INSERT INTO events (judul,tanggal,tempat,gambar,keterangan) VALUES ('$judul','$tanggal','$tempat','$nama_gambar','$keterangan')";
			}else{
				//attempt insert query execution
				$sql = "INSERT INTO events (judul,tanggal,tempat,gambar,keterangan,divisi) VALUES ('$judul','$tanggal','$tempat','$nama_gambar','$keterangan','$divisi')";
			}
			
			if(mysqli_query($con, $sql)){
				echo "<center>Records added successfully.</center>";
			} else{
				echo "<center>ERROR: Could not able to execute $sql.</center>" . mysqli_error($con);
			}
		}
		
		//close connection
		mysqli_close($con);
		
		//redirect page
		echo "<meta http-equiv='refresh' content='1; url=tampilEvent.php'>";
	}else{
		echo "<meta http-equiv='refresh' content='1; url=../halaman_user/login.php'>";
	}
?>
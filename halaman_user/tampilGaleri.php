		<?php include 'layout/header.php'; ?>
		
		<section style="padding-top:100px; min-height:75vh;">
			<div class="container">
				<div class="row text-center inner">
					<?php
						include("../koneksi.php");
						
						$hal=isset($_GET['hal']) ? $_GET['hal']:1;
						$batas=6;
						$posisi=($hal-1)* $batas;
								
						// attempt select query execution
						$sql=mysqli_query($con,"SELECT * from galeri order by idGaleri desc limit $posisi, $batas");
						while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
							//MENGATUR FORMAT DATA KELUARAN
							$tanggal=new DateTime($data['tanggal']);
					?>
					
					<div class="col-md-6">
						<div class="thumbnail">
							<div class="image view view-first">					
								<img style="width: 100%; display: block;" src="../img/uploads/<?php echo $data['gambar'];?>" alt="image" />
								
								<div class="mask">
									<p><?php echo $tanggal->format('l, jS F Y');?></p>
									<!--<div class="tools tools-bottom">
										<a href="#" onclick="javascript:showNew()" ><i class="fa fa-expand"></i></a>
										<a href="#"><i class="fa fa-download"></i></a>
									</div>-->
								</div>
							</div>
							  
							<div class="caption">
								<p><?php echo $data['caption'];?></p>
							</div>
						</div>
					</div>
					
					<!-- SHOW EXPAND IMAGE //
					<div id="myModal" class="expand">
						<!--CONTENT //
						<div class="expand-content">
							<div class="expand-body">
								<span class="close" onclick="javascript:hideNew()">×</span>
								<img style="width: 100%; display: block;" src="../img/uploads/<?php echo $data['gambar'];?>" alt="image" />
							</div>
						</div>
					</div>
					<!-- END OF SHOW EXPAND IMAGE -->
					
					<?php
						}
					?>
                </div>
			<?php
				$semua=mysqli_query($con,"select * from galeri");
				$jmldata= mysqli_num_rows($semua);
				$jmlhal=ceil($jmldata/$batas);
				$sebelum=$hal-1;
				$berikut=$hal+1;
					
				echo "<div class='paging'>";
				//bagian 1
				if($hal>1){
					echo "<span class='btn btn-primary'><a href='tampilGaleri.php?&hal=1'>First</a></span>";
					echo "<span class='btn btn-primary'><a href='tampilGaleri.php?&hal=$sebelum'>Before</a></span>";
				}else{
					echo "<span class='btn btn-primary'>First</span>";
					echo "<span class='btn btn-primary'>Before</span>";
				}
					
				//bagian 2
				for($i=1;$i<=$jmlhal;$i++){
					if($i==$hal){
						echo "<span class='btn btn-info'><u>$i</u></span>";
					}else{
						echo "<span class='btn btn-info'><a href='tampilGaleri.php?&hal=$i'>$i</a></span>";
					}
				}
					
				//bagian 3
				if($hal<$jmlhal){
					echo "<span class='btn btn-primary'><a href='tampilGaleri.php?&hal=$berikut'>Next</a></span>";
					echo "<span class='btn btn-primary'><a href='tampilGaleri.php?&hal=$jmlhal'>Last</a></span>";
				}else{
					echo "<span class='btn btn-primary'>Next</span>";
					echo "<span class='btn btn-primary'>Last</span>";
				}
				echo "</div><br>";
			?>
			</div>
		</section>
		
		<?php include 'layout/footer.php'; ?>
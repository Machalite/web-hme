	<?php include 'layout/header.php'; ?>
	
	<section style="padding-top:100px">
		<div class="container">
			<div class="row text-center inner">
				<!-- CONTENT -->
				<div class="col-sm-8">
					<div class="news-content">
						<?php
							include("../koneksi.php");
							//attempt supdate query execution
							mysqli_query($con,"update berita set dibaca=dibaca+1 where idBerita='$_GET[id]'");
							
							
							//attempt select query execution
							$sql=mysqli_query($con,"SELECT * from berita where idBerita='$_GET[id]'");
							$data=mysqli_fetch_array($sql,MYSQLI_ASSOC);
							
							//MENGATUR FORMAT DATA KELUARAN
							$tanggal=new DateTime($data['tanggal']);
						?>
						<hr>
							<h2 class="feature-content-title gray-text">
								<?php echo $data['judul'];?>
							</h2>
						<hr>
						
						<img class="zoomIn animated" src="../img/uploads/<?php echo $data['gambar'];?>" alt="Image">
						
						<hr>
							<span class="tanggal">
								-<?php echo $tanggal->format('l, jS F Y');?>-
							</span>
						<hr>
						
						<p class="news-content-description">
							<?php echo $data['isi'];?>
						</p>
					</div>
				</div>
				<!-- END OF CONTENT -->
				
				<!-- SIDEBAR BERITA TERBARU -->
				<div class="col-sm-4">
					<div class="news-content slideInRight animated">
						<h2 class="feature-content-title gray-text">Berita Terbaru</h2>
						<hr>
						<ul class="terbaru">
							<?php include 'beritaTerbaru.php'; ?>
						</ul>
					</div>
				</div>
				<!-- END OF SIDEBAR BERITA TERBARU -->
				
				<!-- SIDEBAR BERITA TERPOPULER-->
				<div class="col-sm-4">
					<div class="news-content slideInRight animated">
						<h2 class="feature-content-title gray-text">Berita Terpopuler</h2>
						<hr>
						<ul class="terbaru">
							<?php include 'beritaTerpopuler.php'; ?>
						</ul>
					</div>
				</div>
				<!-- END OF SIDEBAR BERITA TERPOPULER -->
			</div>
		</div>
	</section>

	<?php include 'layout/footer.php'; ?>
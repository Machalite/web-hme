<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<!-- META TAG -->
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Website of Himpunan Mahasiswa Elektro FT UAJ" />
	<meta name="keywords" content="HME, Fakultas Teknik, Unika Atmajaya, Teknik Elektro" />
	<!-- END OF META TAG -->
	
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../css/menu.css" />
	<link rel="stylesheet" type="text/css" href="../css/footer.css" />
	<link rel="stylesheet" type="text/css" href="../css/component.css" />
	<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css"> 
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css" rel="stylesheet">
	<!-- Animate -->
	<link rel="stylesheet" type="text/css" href="../css/animate.css" rel="stylesheet">
	<!-- END OF CSS -->
	
	<link rel="shortcut icon" type="image/png" href="../img/favicon.ico"/>
		
	<title>Himpunan Mahasiswa Elektro</title>
</head>
	
<body>
	<!-- NAVIGATION MENU -->
	<header>
		<ul id="gn-menu" class="gn-menu-main">
			<li class="gn-trigger">
				<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
				<nav class="gn-menu-wrapper">
					<div class="gn-scroller">
						<ul class="gn-menu">
							<li><a href="../index.php" class="gn-icon"><i class="fa fa-home"></i> Home</a></li>
							<li><a href="tampilEvent.php" class="gn-icon"><i class="fa fa-calendar"></i> Events</a></li>
							<li><a href="tampilBerita.php" class="gn-icon"><i class="fa fa-newspaper-o"></i> News</a></li>
							<li><a href="tampilGaleri.php" class="gn-icon"><i class="fa fa-picture-o"></i> Gallery</a>
						</ul>
					</div><!-- /gn-scroller -->
				</nav>
			</li>
			<li><a class="codrops-icon" href="login.php"><i class="fa fa-lock"></i><span>Login</span></a></li>
		</ul>
	</header>
	<!-- END OF NAVIGATION MENU -->
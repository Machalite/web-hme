	<!-- Start Footer Area -->
	<footer>
		<div class="container">
			<h2>Connect with Us</h2> 
			<h3 id="back-top" style="float:right"><a href="#top"><i class="fa fa-arrow-up"></i></a></h3>
			<div class="component">   
				<a href="https://id-id.facebook.com/hme.jaya" target="_blank" class="icon icon-cube facebook">facebook</a>  
				<a href="http://www.imgrum.net/user/hmeftuaj/1547605990" target="_blank" class="icon icon-cube instagram">instagram</a>  
				<a href="http://apcom.atmajaya.org/index.php" target="_blank" class="icon icon-cube apcom">APCOM</a>
				<br>
				<p><small>Copyright &copy 2016 by Himpunan Mahasiswa Elektro FT UAJ</small></p>         
			</div>  
		</div>	
	</footer>
	<!-- End of Footer Area -->

	<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="../js/jquery-1.11.1.js"></script>

    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="../js/bootstrap.js"></script>

    <!-- CUSTOM SCRIPTS  -->
    <script src="../js/custom.js"></script>
	
	<!-- SCROLL TO TOP -->
	<script>
	$(document).ready(function(){

		// hide #back-top first
		$("#back-top").hide();
		
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});

			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});

	});
	</script>
	
	<!-- JS FOR WHAT'S NEW -->
	<script>
		function showNew(){
			var modal=document.getElementById("myModal");
			modal.style.display="block";
		}

		function hideNew(){
			var modal=document.getElementById("myModal");
			modal.style.display="none";
		}
	</script>
	
	<script src="../js/modernizr.custom.js"></script>
	<script src="../js/classie.js"></script>
	<script src="../js/gnmenu.js"></script>
	<script>
		new gnMenu( document.getElementById( 'gn-menu' ) );
	</script>
	</body>
</html>
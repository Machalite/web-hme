	<?php 
		// MENCEGAH USER MASUK MELALUI URL TANPA LOGIN
		@session_start();
		
		if(!empty($_SESSION['username']) and !empty($_SESSION['password'])){
			include 'layout/header.php'; 	
			if($_SESSION['divisi']!="Eksternal"&&$_SESSION['divisi']!="E-Society"&&$_SESSION['divisi']!="")
			{?>
				<script>alert("Anda tidak berhak mengakses halaman ini.")</script>
				<script type="text/javascript">location.href = 'index.php';</script>
			<?php
			}
	?> 
	
	<!-- PAGE CONTENT -->
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3>Ubah Data Sekolah </h3>
				</div>
            </div>
		</div>
				
		<div class="clearfix"></div>		

		<div class="row">					
			<div class="col-md-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<!--<a class="close-link"><i class="fa fa-close"></i></a>-->
							</li>
							<li class="dropdown">
								<!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a></li>
									<li><a href="#">Settings 2</a></li>
								</ul>-->
							</li>
							<li>
								<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>							
						<div class="clearfix"></div>
					</div>
							
					<div class="x_content">
					<br />
					
					<?php
						include("../koneksi.php");
						
						// attempt select query execution
						$sql=mysqli_query($con,"SELECT * from sekolah where kd_sekolah='$_GET[id]'");
						$data=mysqli_fetch_array($sql,MYSQLI_ASSOC);
					?>
							
						<form name="edit" method="post" action="ubahSekolah_proses.php" enctype="multipart/form-data" class="form-horizontal form-label-left">
							<input type="hidden" name="kd_sekolah" value="<?php echo $data['kd_sekolah']; ?>">
							<input type="hidden" name="bukti_lama" value="<?php echo $data['bukti_bayar']; ?>">
							<input type="hidden" name="pass_lama" value="<?php echo $data['user_pass']; ?>">

							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									User ID
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="userId" class="form-control"
									value="<?php echo $data['user_id']; ?>">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Password
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="userPass" class="form-control" placeholder="Masukkan password baru"
									value="<?php echo $data['user_pass']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Nama sekolah <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="namaSekolah" class="form-control" required
									value="<?php echo $data['nama_sekolah']; ?>">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Telepon sekolah
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="telpSekolah" class="form-control"
									value="<?php echo $data['telp_sekolah']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Alamat sekolah
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="alamatSekolah" class="form-control"
									value="<?php echo $data['alamat_sekolah']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Nama guru pendamping
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="namaGuru" class="form-control"
									value="<?php echo $data['nama_guru']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Telepon guru
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="telpGuru" class="form-control"
									value="<?php echo $data['telp_guru']; ?>">
								</div>
							</div>
					
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Foto Bukti pembayaran
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<img src="../../apcom.atmajaya.org/img/apcom/bukti/<?php echo $data['bukti_bayar'];?>" width="100px">
									<input type="file" name="buktiBayar" class="form-control">
								</div>
							</div>

							<div class="ln_solid"></div>
									
							<div class="form-group">
								<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-2">
									<button type="submit" class="btn btn-primary">Ubah</button>
									<a href="tampilSekolah.php">
										<button type="button" class="btn btn-danger">Batal</button>
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END OF PAGE CONTENT -->
	
	<?php require 'layout/footer.php'; ?>
			
	<!-- JS -->
	<!-- jQuery -->
    <script src="../js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../js/nprogress.js"></script>
    <!-- Datatables -->
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/dataTables.buttons.min.js"></script>
    <script src="../js/buttons.bootstrap.min.js"></script>
    <script src="../js/buttons.flash.min.js"></script>
    <script src="../js/buttons.html5.min.js"></script>
    <script src="../js/buttons.print.min.js"></script>
    <script src="../js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/dataTables.keyTable.min.js"></script>
    <script src="../js/dataTables.responsive.min.js"></script>
    <script src="../js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.scroller.min.js"></script>
    <script src="../js/jszip.min.js"></script>
    <script src="../js/pdfmake.min.js"></script>
    <script src="../js/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
	<!-- END OF JS -->
  </body>
</html>
<?php
	}else{
		echo "<meta http-equiv='refresh' content='1; url=../halaman_user/login.php'>";
	}
?>
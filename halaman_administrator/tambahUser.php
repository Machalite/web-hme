	<?php 
		@session_start();
		
		if(!empty($_SESSION['username']) and !empty($_SESSION['password'])){ 
			require 'layout/header.php'; 
	?>
	
	<!-- PAGE CONTENT -->
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3>Tambah User </h3>
				</div>
            </div>
		</div>
				
		<div class="clearfix"></div>		

		<div class="row">					
			<div class="col-md-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<!--<a class="close-link"><i class="fa fa-close"></i></a>-->
							</li>
							<li class="dropdown">
								<!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a></li>
									<li><a href="#">Settings 2</a></li>
								</ul>-->
							</li>
							<li>
								<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>							
						<div class="clearfix"></div>
					</div>
							
					<div class="x_content">
					<br />
						<!-- FORM -->
						<form name="tambah" method="post" action="tambahUser_proses.php" enctype="multipart/form-data" class="form-horizontal form-label-left">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Username <span class="required">*</span>
								</label>
								<div class="col-md-3 col-sm-9 col-xs-12">
									<input type="text" name="username" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Password <span class="required">*</span>
								</label>
								<div class="col-md-3 col-sm-9 col-xs-12">
									<input type="password" name="password" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Ulangi password <span class="required">*</span>
								</label>
								<div class="col-md-3 col-sm-9 col-xs-12">
									<input type="password" name="password1" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Nama <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-9 col-xs-12">
									<input type="text" name="nama" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Email <span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-9 col-xs-12">
									<input type="email" name="email" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Gambar
								</label>
								<div class="col-md-4 col-sm-9 col-xs-12">
									<input type="file" name="gambar" class="form-control">
								</div>
							</div>
							
							<?php
								if($_SESSION['divisi']=="E-Society"||$_SESSION['divisi']=="")
								{
								$pilihdiv = array(
								   "Eksternal" => "Eksternal",
								   "Internal" => "Internal",
								   "Infrastruktur" => "Infrastruktur",
								   "WSE" => "WSE",
								   "E-Society" => "E-Society",
								   "Anggota" => "Anggota",
								);
								}
								else
								{
									$pilihdiv = array(
									$_SESSION['divisi'] => $_SESSION['divisi']
									);
								}
							?>
							
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-3 col-xs-12">
									Divisi <span class="required">*</span>
								</label>
								
								<div class="col-md-3 col-sm-9 col-xs-12">
									<select name="divisi" id="listBox" class="form-control">
									   <?php foreach($pilihdiv as $individualDiv=>$div){?>
									   <option value=<?php echo $div; ?>> <?php echo $individualDiv; ?></option>
									   <?php } ?>
									</select>
								</div>
							</div>

							<div class="ln_solid"></div>
									
							<div class="form-group">
								<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-2">
									<button type="submit" class="btn btn-primary">Tambah</button>
									<a href="user.php">
										<button type="button" class="btn btn-danger">Batal</button>
									</a>
								</div>
							</div>
						</form>
						<!-- END OF FORM -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END OF PAGE CONTENT -->
	
	<?php require 'layout/footer.php'; ?>
			
	<!-- JS -->
	<!-- jQuery -->
    <script src="../js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../js/nprogress.js"></script>
    <!-- Datatables -->
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/dataTables.buttons.min.js"></script>
    <script src="../js/buttons.bootstrap.min.js"></script>
    <script src="../js/buttons.flash.min.js"></script>
    <script src="../js/buttons.html5.min.js"></script>
    <script src="../js/buttons.print.min.js"></script>
    <script src="../js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/dataTables.keyTable.min.js"></script>
    <script src="../js/dataTables.responsive.min.js"></script>
    <script src="../js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.scroller.min.js"></script>
    <script src="../js/jszip.min.js"></script>
    <script src="../js/pdfmake.min.js"></script>
    <script src="../js/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
	<!-- END OF JS -->
  </body>
</html>

<?php
	}else{
		echo "<meta http-equiv='refresh' content='1; url=login.php'>";
	}
?>
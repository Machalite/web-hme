<?php
	include("../koneksi.php");
	$no=1;
	$divisi=$_SESSION['divisi'];
	$sql;
	
	if($divisi==""){
		// attempt select query execution
		$sql=mysqli_query($con,"SELECT * from galeri order by idGaleri ");
	}else{
		// attempt select query execution
		$sql=mysqli_query($con,"SELECT * from galeri where divisi='$divisi' order by idGaleri ");
	}
	

	while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
		//MENGATUR FORMAT DATA KELUARAN
		$tanggal=new DateTime($data['tanggal']);
?>
	<tr>
		<td width="50"><?php echo $no;?></td>
		<td><img src="../img/uploads/<?php echo $data['gambar'];?>" width="100px"></td>
		<td width="150"><?php echo $tanggal->format('d-m-Y');?></td>
		<td><?php echo $data['caption'];?></td>
		<td width="150">
			<a href="ubahGaleri.php?&id=<?php echo $data['idGaleri']; ?>">
				<button type="button" class="btn btn-primary">Ubah</button>
			</a>
			<a href="hapusGaleri.php?&id=<?php echo $data['idGaleri']; ?>" onClick="return confirm('Hapus berita ini?')">
				<button type="button" class="btn btn-danger">Hapus</button>
			</a>
		</td>
	</tr>
<?php
	$no++;
	}
?>
<?php
	include("../koneksi.php");
	$no=1;
	$divisi=$_SESSION['divisi'];
	$sql;
	
	if($divisi==""){
		// attempt select query execution
		$sql=mysqli_query($con,"SELECT * from berita order by idBerita ");
	}else{
		// attempt select query execution
		$sql=mysqli_query($con,"SELECT * from berita where divisi='$divisi' order by idBerita ");
	}
	

	while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
		//MEMBATASI ISI BERITA YANG DITAMPILKAN SAMPAI 100 HURUF PERTAMA SAJA
		$isi=substr($data['isi'],0,100);
			
		//MENGAMBIL ISI BERITA SAMPAI KETEMU SPASI TERAKHIR SEBELUM 100 HURUF PERTAMA
		$isi=substr($data['isi'],0,strrpos($isi," "));
			
		//MENGATUR FORMAT DATA KELUARAN
		$tanggal=new DateTime($data['tanggal']);
?>
	<tr>
		<td><?php echo $no;?></td>
		<td width="150"><?php echo $data['judul'];?></td>
		<td><?php echo $isi;?> ...</td>
		<td><img src="../img/uploads/<?php echo $data['gambar'];?>" width="100px"></td>
		<td><?php echo $tanggal->format('d-m-Y');?></td>
		<td width="150">
			<a href="ubahBerita.php?&id=<?php echo $data['idBerita']; ?>">
				<button type="button" class="btn btn-primary">Ubah</button>
			</a>
			<a href="hapusBerita.php?&id=<?php echo $data['idBerita']; ?>" onClick="return confirm('Hapus berita ini?')">
				<button type="button" class="btn btn-danger">Hapus</button>
			</a>
		</td>
	</tr>
<?php
	$no++;
	}
?>
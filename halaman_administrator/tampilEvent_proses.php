<?php
	include("../koneksi.php");
	$no=1;
	$divisi=$_SESSION['divisi'];
	$sql;
	
	if($divisi==""){
		// attempt select query execution
		$sql=mysqli_query($con,"SELECT * from events order by idEvent");
	}else{
		// attempt select query execution
		$sql=mysqli_query($con,"SELECT * from events where divisi='$divisi' order by idEvent");
	}
	

	while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
		//MENGATUR FORMAT DATA KELUARAN
		$tanggal=new DateTime($data['tanggal']);
?>
	<tr>
		<td><?php echo $no;?></td>
		<td width="150"><?php echo $data['judul'];?></td>
		<td><?php echo $tanggal->format('d-m-Y');?></td>
		<td width="150"><?php echo $data['tempat'];?></td>
		<td width="150"><?php echo $data['keterangan'];?></td>
		<td><img src="../img/uploads/<?php echo $data['gambar'];?>" width="100px"></td>
		
		<td width="150">
			<a href="ubahEvent.php?&id=<?php echo $data['idEvent']; ?>">
				<button type="button" class="btn btn-primary">Ubah</button>
			</a>
			<a href="hapusEvent.php?&id=<?php echo $data['idEvent']; ?>" onClick="return confirm('Hapus events ini?')">
				<button type="button" class="btn btn-danger">Hapus</button>
			</a>
		</td>
	</tr>
<?php
	$no++;
	}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<!-- META TAG -->
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Website of Himpunan Mahasiswa Elektro FT UAJ" />
	<meta name="keywords" content="HME, Fakultas Teknik, Unika Atmajaya, Teknik Elektro" />
	<!-- END OF META TAG -->
	
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link rel="stylesheet" type="text/css" href="css/footer.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"> 
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" rel="stylesheet">
	<!-- Animate -->
	<link rel="stylesheet" type="text/css" href="css/animate.css" rel="stylesheet">
	<!-- END OF CSS -->
	
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
		
	<title>Himpunan Mahasiswa Elektro</title>
</head>
	
<body>
	<!-- NAVIGATION MENU -->
	<header>
		<ul id="gn-menu" class="gn-menu-main">
			<li class="gn-trigger">
				<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
				<nav class="gn-menu-wrapper">
					<div class="gn-scroller">
						<ul class="gn-menu">
							<li><a href="index.php" class="gn-icon"><i class="fa fa-home"></i> Home</a></li>
							<li><a href="halaman_user/tampilEvent.php" class="gn-icon"><i class="fa fa-calendar"></i> Events</a></li>
							<li><a href="halaman_user/tampilBerita.php" class="gn-icon"><i class="fa fa-newspaper-o"></i> News</a></li>
							<li><a href="halaman_user/tampilGaleri.php" class="gn-icon"><i class="fa fa-picture-o"></i> Gallery</a>
						</ul>
					</div><!-- /gn-scroller -->
				</nav>
			</li>
			<li><a class="codrops-icon" href="halaman_user/login.php"><i class="fa fa-lock"></i><span>Login</span></a></li>
		</ul>
	</header>
	<!-- END OF NAVIGATION MENU -->
	
	<!-- Start Desciption Area -->
	<section class="description">
		<div class="container">
		
		<div class="col-md-12 col-sm-12 col-xs-12 bounceInLeft animated">
			<img src="img/logo.jpg">
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12 bounceInRight animated">
			<div class="content">
				<h1>Himpunan Mahasiswa Elektro</h1> 
				<h3>Fakultas Teknik Unika Atma Jaya Jakarta</h3>
				<p> 
					We are an organization of creative students working together.
					We work hard at developing student's softskill and knowledge.
					<br>Together we achive more!
				</p>
			</div>
		</div>
		</div>
	</section>
	<!-- End of Description Area -->
	
	<!-- Start News Area -->
	<section id="news" class="about-section">
		<div class="container">
			<div class="row text-center inner">
				<?php include 'halaman_user/tampilBerita_index.php'; ?>
			</div>
		</div>
	</section>
	<!-- End of News Area -->
	
	<!-- Start Event Area -->
	<section class="event">
		<div class="container">
			<div id="carousel-example" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example" data-slide-to="1"></li>
					<li data-target="#carousel-example" data-slide-to="2"></li>
				</ol>
								
				<div class="carousel-inner">
					<?php include 'halaman_user/tampilEvent_index.php'; ?>
				</div>
			</div>
		</div>
	</section>
	<!-- End of Event Area -->
	
	<!-- Start Visi & Misi Area -->
	<section class="visi-misi text-center "  >  
		<div class="container">
		<div class="col-lg-6  col-md-6 col-sm-6 visi" data-scroll-reveal="enter from the left after 0.2s">                             
			<span >VISI</span>                                    
			<p>
				HME Unika Atma Jaya sebagai wadah yang digunakan anggotanya untuk mengembangkan kreativitas, professionalisme,
				dan potensi diri sehingga anggotanya bersaing dalam dunia kerja dan membangun kerjasama yang baik antara anggota, 
				fakultas, dan universitas
			</p>              
		</div>
					
		<div class="col-lg-6  col-md-6 col-sm-6 misi" data-scroll-reveal="enter from the right after 0.2s">    
			<span >MISI</span>               
			<p>
				<UL>
				<LI> Meningkatkan kebersamaan dan kekeluargaan diantara seluruh keluarga besar HME melalui acara - acara HME maupun acara - acara diluar HME</LI>
				<LI> Meningkatkan keaktifan anggota HME dalam berbagai kegiatan di dalam dan di luar HME</LI>
				<LI> Mendorong peningkatan <i>softskill</i> dan <i>hardskill</i> anggota HME UAJ </LI>
				<LI> Menjalin hubungan dan komunikasi yang baik antar anggota, alumni, fakultas, dan universitas</LI>
				</UL>
			</p>                     
		</div>   
		</div>
	</section>
	<!-- End of Visi & Misi Area -->  
	
	<!-- Start Footer Area -->
	<footer>
		<div class="container">
			<h2>Connect with Us</h2>  
			<h3 id="back-top" style="float:right"><a href="#top"><i class="fa fa-arrow-up"></i></a></h3>			
			<div class="component">   
				<a href="https://id-id.facebook.com/hme.jaya" target="_blank" class="icon icon-cube facebook">facebook</a>  
				<a href="http://www.imgrum.net/user/hmeftuaj/1547605990" target="_blank" class="icon icon-cube instagram">instagram</a>  
				<a href="http://apcom.atmajaya.org/index.php" target="_blank" class="icon icon-cube apcom">APCOM</a>     
				<!--<a href="#" class="icon icon-cube youtube">youtube</a>-->
				<br>
				<p><small>Copyright &copy 2016 by Himpunan Mahasiswa Elektro FT UAJ</small></p>         
			</div>  
			
		</div>	
	</footer>
	<!-- End of Footer Area -->

	<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="js/jquery-1.11.1.js"></script>

    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="js/bootstrap.js"></script>

    <!-- CUSTOM SCRIPTS  -->
    <script src="js/custom.js"></script>
	
	<!-- SCROLL TO TOP -->
	<script>
	$(document).ready(function(){

		// hide #back-top first
		$("#back-top").hide();
		
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});

			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});

	});
	</script>
	
	<script src="js/modernizr.custom.js"></script>
	<script src="js/classie.js"></script>
	<script src="js/gnmenu.js"></script>
	<script>
		new gnMenu( document.getElementById( 'gn-menu' ) );
	</script>
	</body>
</html>
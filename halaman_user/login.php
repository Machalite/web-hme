<!DOCTYPE html>
<html lang="en">
<head>
	<!-- META TAG -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- END OF META TAG -->

    <!-- CSS -->
	<!-- Bootstrap -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../css/font-awesome.css" rel="stylesheet">	
    <!-- Custom Theme Style -->
    <link href="../css/custom.css" rel="stylesheet">
	<!-- END OF CSS -->
	
	<link rel="shortcut icon" type="image/png" href="../img/favicon.ico"/>
	
	<title>Himpunan Mahasiswa Elektro</title>
</head>

<body class="login">
    <div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form method="post" action="login_proses.php">
						<h1>Login</h1>
						
						<div>
							<input type="text" name="username" class="form-control" placeholder="Username" required="" />
						</div>
						
						<div>
							<input type="password" name="password" class="form-control" placeholder="Password" required="" />
						</div>
						
						<div>
							<button type="submit" class="btn btn-primary">Log in</button>
							<a href="../index.php"><button type="button" class="btn btn-danger">Cancel</button></a>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<div>
								<h1>
									Himpunan Mahasiswa Elektro
								</h1>
								<p>Copyright &copy 2016 by Himpunan Mahasiswa Elektro FT UAJ</p>
							</div>
						 </div>
					</form>
				</section>
			</div>
		</div>
    </div>
</body>
</html>
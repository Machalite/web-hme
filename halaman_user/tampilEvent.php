	<?php include 'layout/header.php'; ?>
	
	<section style="padding-top:100px">
		<div class="container">
			<div class="row text-center inner">
				<?php
					include("../koneksi.php");
					
					$hal=isset($_GET['hal']) ? $_GET['hal']:1;
					$batas=3;
					$posisi=($hal-1)* $batas;
							
					// attempt select query execution
					$sql=mysqli_query($con,"SELECT * from events order by tanggal desc limit $posisi, $batas");
					while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
						//MENGATUR FORMAT DATA KELUARAN
						$tanggal=new DateTime($data['tanggal']);
				?>
				<div class="col-sm-4">
					<div class="news-content">
						<img class="zoomIn animated" src="../img/uploads/<?php echo $data['gambar'];?>" alt="Image">
						
						<hr>
							<h2 class="feature-content-title gray-text"><?php echo $data['judul'];?></h2>
						<hr>
						
						<p class="news-content-description">
							<i class="fa fa-calendar"></i> Tanggal : <?php echo $tanggal->format('l, jS F Y');?>
							<br>
							<i class="fa fa-building"></i> Tempat : <?php echo $data['tempat'];?>
							<br>
							<i class="fa fa-info"></i> Keterangan : <?php echo $data['keterangan'];?>
						</p>
					</div>
				</div>
				<?php
					}
				?>
			</div>
			<?php
				$semua=mysqli_query($con,"select * from events");
				$jmldata= mysqli_num_rows($semua);
				$jmlhal=ceil($jmldata/$batas);
				$sebelum=$hal-1;
				$berikut=$hal+1;
					
				echo "<div class='paging'>";
				//bagian 1
				if($hal>1){
					echo "<span class='btn btn-primary'><a href='tampilEvent.php?&hal=1'>First</a></span>";
					echo "<span class='btn btn-primary'><a href='tampilEvent.php?&hal=$sebelum'>Before</a></span>";
				}else{
					echo "<span class='btn btn-primary'>First</span>";
					echo "<span class='btn btn-primary'>Before</span>";
				}
					
				//bagian 2
				for($i=1;$i<=$jmlhal;$i++){
					if($i==$hal){
						echo "<span class='btn btn-info'><u>$i</u></span>";
					}else{
						echo "<span class='btn btn-info'><a href='tampilEvent.php?&hal=$i'>$i</a></span>";
					}
				}
					
				//bagian 3
				if($hal<$jmlhal){
					echo "<span class='btn btn-primary'><a href='tampilEvent.php?&hal=$berikut'>Next</a></span>";
					echo "<span class='btn btn-primary'><a href='tampilEvent.php?&hal=$jmlhal'>Last</a></span>";
				}else{
					echo "<span class='btn btn-primary'>Next</span>";
					echo "<span class='btn btn-primary'>Last</span>";
				}
				echo "</div><br>";
			?>
		</div>
	</section>

	<?php include 'layout/footer.php'; ?>
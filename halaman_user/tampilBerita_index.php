<?php
	include("koneksi.php");
	
	// attempt select query execution
	$sql=mysqli_query($con,"SELECT * from berita order by idBerita desc limit 3");

	while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
		//MEMBATASI ISI BERITA YANG DITAMPILKAN SAMPAI 200 HURUF PERTAMA SAJA
		$isi=substr($data['isi'],0,200);
		
		//MENGAMBIL ISI BERITA SAMPAI KETEMU SPASI TERAKHIR SEBELUM 200 HURUF PERTAMA
		$isi=substr($data['isi'],0,strrpos($isi," "));
?>
	<div class="col-sm-4">
		<div class="news-content index">
			<img src="img/uploads/<?php echo $data['gambar'];?>" alt="Image">
			<hr>
			<h4 class="feature-content-title gray-text"><?php echo $data['judul'];?></h4>
			<hr>
			<p class="news-content-description">
				<?php echo $isi;?>...
			</p>
			<a href="halaman_user/berita.php?&id=<?php echo $data['idBerita']; ?>" class="feature-content-link blue-btn">See Details</a>
		</div>
	</div>
<?php
	}
?>
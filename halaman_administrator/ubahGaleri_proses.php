<?php
	// MENCEGAH USER MASUK MELALUI URL TANPA LOGIN
	session_start();
		
	if(!empty($_SESSION['username']) and !empty($_SESSION['password'])){
		include("../koneksi.php");
		
		// MENDAPATKAN INFORMASI MENGENAI GAMBAR YANG DIUPLOAD (NAMA,LOKASI, DAN TIPE)
		$nama_gambar=$_FILES['gambar']['name'];
		$lokasi_gambar=$_FILES['gambar']['tmp_name'];
		$tipe_gambar=$_FILES['gambar']['type'];
		
		// DEFAULT TANGGAL DIISI OTOMATIS(HARI INI)
		$tanggal=date('Ymd');

		//escape user inputs for security
		$caption = mysqli_real_escape_string($con, $_POST['caption']);
	 
		if($lokasi_gambar==""){
			//attempt update query execution
			$sql = "UPDATE galeri set caption='$caption',tanggal='$tanggal' where idGaleri='$_POST[id]'";
			
			if(mysqli_query($con, $sql)){
				echo "<center>Records changed successfully.</center>";
			} else{
				echo "<center>ERROR: Could not able to execute $sql.</center>" . mysqli_error($con);
			}
		}else{
			$data=mysqli_fetch_array(mysqli_query($con,"SELECT * from galeri where idGaleri='$_POST[id]'"),MYSQLI_ASSOC);
			
			//JIKA SEBELUMNYA SUDAH ADA GAMBAR, GAMBAR YANG LAMA DIHAPUS
			if($data['gambar']!=""){
				unlink("../img/uploads/$data[gambar]");
			}
			
			//MEMINDAHKAN GAMBAR YANG DIUPLOAD KE FOLDER IMG
			move_uploaded_file($lokasi_gambar,"../img/uploads/$nama_gambar");
			
			//attempt update query execution
			$sql = "UPDATE galeri set gambar='$nama_gambar',caption='$caption',tanggal='$tanggal' where idGaleri='$_POST[id]'";
			
			if(mysqli_query($con, $sql)){
				echo "<center>Records changed successfully.</center>";
			} else{
				echo "<center>ERROR: Could not able to execute $sql.</center>" . mysqli_error($con);
			}
		}
		
		//close connection
		mysqli_close($con);
		
		//redirect page
		echo "<meta http-equiv='refresh' content='1; url=tampilGaleri.php'>";
	}else{
		echo "<meta http-equiv='refresh' content='1; url=../halaman_user/login.php'>";
	}
?>
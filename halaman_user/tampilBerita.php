	<?php include 'layout/header.php'; ?>
	
	<section style="padding-top:100px">
		<div class="container">
			<div class="row text-center inner">
				<div class="col-sm-8">
				<?php
					include("../koneksi.php");
					
					$hal=isset($_GET['hal']) ? $_GET['hal']:1;
					$batas=5;
					$posisi=($hal-1)* $batas;
							
					// attempt select query execution
					$sql=mysqli_query($con,"SELECT * from berita order by idBerita desc limit $posisi, $batas");
					while($data=mysqli_fetch_array($sql,MYSQLI_ASSOC)){
						//MEMBATASI ISI BERITA YANG DITAMPILKAN SAMPAI 350 HURUF PERTAMA SAJA
						$isi=substr($data['isi'],0,350);
								
						//MENGAMBIL ISI BERITA SAMPAI KETEMU SPASI TERAKHIR SEBELUM 350 HURUF PERTAMA
						$isi=substr($data['isi'],0,strrpos($isi," "));
				?>
				
					<div class="news-content">
						<h2 class="feature-content-title gray-text"><?php echo $data['judul'];?></h2>
						
						<div class="col-sm-4">
							<img class="zoomIn animated" src="../img/uploads/<?php echo $data['gambar'];?>" alt="Image">
						</div>
						
						<div class="col-sm-8">
							<p class="news-content-description">
								<?php echo $isi;?>...
								<br>
								<a href="berita.php?&id=<?php echo $data['idBerita']; ?>" class="btn btn-primary">See Details</a>
							</p>
						</div>
					</div>
					
					<br>
					<hr>
				<?php
					}
					
					$semua=mysqli_query($con,"select * from berita");
					$jmldata= mysqli_num_rows($semua);
					$jmlhal=ceil($jmldata/$batas);
					$sebelum=$hal-1;
					$berikut=$hal+1;
					
					echo "<div class='paging'>";
					//bagian 1
					if($hal>1){
						echo "<span class='btn btn-primary'><a href='tampilBerita.php?&hal=1'>First</a></span>";
						echo "<span class='btn btn-primary'><a href='tampilBerita.php?&hal=$sebelum'>Before</a></span>";
					}else{
						echo "<span class='btn btn-primary'>First</span>";
						echo "<span class='btn btn-primary'>Before</span>";
					}
					
					//bagian 2
					for($i=1;$i<=$jmlhal;$i++){
						if($i==$hal){
							echo "<span class='btn btn-info'><u>$i</u></span>";
						}else{
							echo "<span class='btn btn-info'><a href='tampilBerita.php?&hal=$i'>$i</a></span>";
						}
					}
					
					//bagian 3
					if($hal<$jmlhal){
						echo "<span class='btn btn-primary'><a href='tampilBerita.php?&hal=$berikut'>Next</a></span>";
						echo "<span class='btn btn-primary'><a href='tampilBerita.php?&hal=$jmlhal'>Last</a></span>";
					}else{
						echo "<span class='btn btn-primary'>Next</span>";
						echo "<span class='btn btn-primary'>Last</span>";
					}
					echo "</div><br>";
				?>
				</div>
				
				<!-- SIDEBAR BERITA TERBARU -->
				<div class="col-sm-4">
					<div class="news-content slideInRight animated">
						<h2 class="feature-content-title gray-text">Berita Terbaru</h2>
						<hr>
						<ul class="terbaru">
							<?php include 'beritaTerbaru.php'; ?>
						</ul>
					</div>
				</div>
				<!-- END OF SIDEBAR BERITA TERBARU -->
				
				<!-- SIDEBAR BERITA TERPOPULER-->
				<div class="col-sm-4" style="float:right;">
					<div class="news-content slideInRight animated">
						<h2 class="feature-content-title gray-text">Berita Terpopuler</h2>
						<hr>
						<ul class="terbaru">
							<?php include 'beritaTerpopuler.php'; ?>
						</ul>
					</div>
				</div>
				<!-- END OF SIDEBAR BERITA TERPOPULER -->
			</div>
		</div>
	</section>

	<?php include 'layout/footer.php'; ?>